# ansible-hetzner-ubuntu-1804-k8s-setup

Ansible Playbook to go from a fresh Hetzner dedicated machine w/ Ubuntu 18.04 LTS installed to a single node Kubernetes cluster, leveraging kubeadm.